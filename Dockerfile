FROM ubuntu:latest

RUN apt-get update
RUN apt-get install --assume-yes python3 python3-pip
RUN pip3 install --trusted-host pypi.python.org -r requirements.txt